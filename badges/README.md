## Pipeline

Whe have created a [Pipeline](https://docs.gitlab.com/ee/ci/pipelines/) for [avidaR](https://gitlab.com/fortunalab/avidaR/) which is launched everytime a push is executed (or a commit from gilab web UI). This Pipeline is defined in [here](https://gitlab.com/fortunalab/avidaR/-/blob/main/.gitlab-ci.yml).

### Script

We have programmed three independent stage on it:

- **test:** avidaR is **installed** from gitlab, **tests** are run on it, and **coverage** test tool is executed. No artifacts are stored.

- **build:** Package is **built** without vignettes and manual, and **R CMD check is run**. The **package tarball** is stored as an **artifact** for later downloads.

- **check:** CMD check --as-cran is executed. No artifacts are stored.

![script](./img/yml_script.png)


### Monitoring

By now, we **take a look** at [avidaR repo --> main menu -> CI/CD -> Pipelines](https://gitlab.com/fortunalab/avidaR/-/pipelines) to monitor Pipeline status.

![pipeline](./img/pipeline_monitor.png)

If any o these stages are unsuccessfully executed, we can review them to get information about bugs or notes.

![failed](./img/pipeline_failed.png)

![monitor log](./img/monitor_log.png)

Otherwise we can continue developing and updating our source code noramlly. This is useful to not to be checking manually our code in every uptade we do, leaving for the end the task of checking before submitting to CRAN.

The pipeline might be improved by publishin/sending the logs and reports to be reviewed by maintainers, and/or in order to automate other tasks.

<i>Note: By default only 400 minutes of CPU execution for pipelines are availabe for each repository, if this time is reached gitlab will suggest to buy more time...</i>

### Artifacts

The tarball file of the package stored at public/ folder during build stage, may be donwloaded in order to install, check, or submit it to CRAN.

![donwload artifacts](./img/download_artifacts.png)

## Badges

### What are badges?

<i>"Badges are a unified way to present condensed pieces of information about your projects. They consist of a small image and a URL that the image points to. Examples for badges can be the pipeline status, test coverage, latest release, or ways to contact the project maintainers..."</i>.

Read more about [badges](https://docs.gitlab.com/ee/user/project/badges.html).

![badgets](./img/readme_badgets.png)

**Pipeline status** and **code coverage** percentage badges are populated with data extracted from the pipeline output logs created during execution.

Note: **CRAN** and **Downloads** badges are populate from **CRAN API**. In the moment of writing this document, avidaR package is still in development process and has not yet being sent to CRAN repository.

### Code

In order to show the badges it in the README.md file, whe have added on top of the document following code:
```
<!-- badges: start -->

[![pipeline status](https://gitlab.com/fortunalab/avidaR/badges/main/pipeline.svg)](https://gitlab.com/fortunalab/avidaR/-/commits/main) [![coverage report](https://gitlab.com/fortunalab/avidaR/badges/main/coverage.svg)](https://gitlab.com/fortunalab/avidaR/-/commits/main) [![Version on
CRAN](https://www.r-pkg.org/badges/version/avidaR?color=brightgreen)](https://cran.r-project.org/package=avidaR) [![Total downloads on
CRAN](https://cranlogs.r-pkg.org/badges/grand-total/avidaR?color=brightgreen)](https://cran.r-project.org/package=avidaR) 

<!-- badges: end -->
```

### Settings

In repository [menu -> Settings -> CI/CD](https://gitlab.com/fortunalab/avidaR/-/settings/ci_cd), do expand **General Pieplines** section. 

Fill in **Test coverage parsing** field with the regular expression:

```
avidaR Coverage: (\d+\.\d+\%)
```

![Test coverage parsing](./img/test_coverage_parsing.png)


Then clic on **Save Changes** button.

Next, browse to [CI/CD -> Pipelines](https://gitlab.com/fortunalab/avidaR/-/pipelines) and clic on **Run pipeline** button.

![Run pipeline](./img/run_pipeline.png)

Finally, we browse/refresh README.md to check out coverage percentage updated.

<i>Note: Value coverage percentage is captured from outuput generated by **R -e 'covr::package_coverage()'** command at **test stage**.</i>

![avidaR Coverage](./img/avidaR_coverage.png)

